package ru.rumsd.usertest;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class UserRepository {

    private List<User> userRepository = new CopyOnWriteArrayList<>();

    public void addUser(User user) {
        userRepository.add(user);
    }

    public User getUser(int userID) {
        return userRepository.stream()
                .filter(user -> user.getId() == userID)
                .findAny()
                .orElse(null);
    }

    public List<User> getOrderedUsers(){
        return userRepository.stream()
                .sorted(Comparator.comparing(User::getId))
                .collect(Collectors.toList());
    }
}