package ru.rumsd.reversetest;

public class ReverseTest {

    private void reverseByteArray(byte[] arr){

        for (byte b: arr) {
            byte rev = (byte) (Integer.reverse(b) >>> (Integer.SIZE - Byte.SIZE));
            System.out.print(String.format("%8s", Integer.toBinaryString(rev & 0xFF)).replace(' ', '0') + " ");
        }
        System.out.println();
    }
}
