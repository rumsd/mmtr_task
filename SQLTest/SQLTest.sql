-- 1.	Выводит имена всех продавцов, имеющих заказы от Seamens
select distinct sp.name from Salesperson sp
join Orders o on o.salesperson_id = sp.id
join Customer c on o.cust_id = c.id
where c.name = 'Seamens';

-- 2.	Выводит имена всех продавцов, не имеющих заказы от Seamens
select distinct sp.name from Salesperson sp
join Orders o on o.salesperson_id = sp.id
join Customer c on o.cust_id = c.id
where c.name <> 'Seamens';

-- 3.	Выводит имена всех продавцов, имеющих два и более заказов
select sp.name from Salesperson sp
join Orders o on o.salesperson_id = sp.id
GROUP BY sp.name
HAVING COUNT(o.salesperson_id) >= 2;

-- 4.	Вставляет в таблицу TopSales (Name, Age) записи обо всех продавцах, зарплата которых больше 100 000.
INSERT INTO TopSales (name, age)
SELECT sp.name, sp.age
FROM Salesperson sp
where sp.salary > 100000