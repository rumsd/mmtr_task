from flask import render_template
from flask import request
import json

from app import app


@app.route("/")
@app.route("/index")
def index():
    return render_template("index.html")


@app.route("/hello", methods=["GET", "POST"])
def hello():
    if request.method == "GET":
        with open("storage.txt") as j:
            data = json.load(j)
        return render_template("hello.html", data=data, title="Hello Page")


@app.route("/create_record", methods=["POST"])
def create_record():
    data = {
        "username": request.form.get("username"),
        "color": request.form.get("color"),
        "is18": request.form.get("is18"),
        "daytime": request.form.get("daytime"),
    }

    result = {
        "result": "",
        "errors": []
    }

    if not data["username"] or not str(data["username"]).strip():
        result["errors"].append({
            "field": "name",
            "message": "name is required"
        })

    if not data["daytime"]:
        result["errors"].append({
            "field": "name",
            "message": "daytime is required"
            }
        )

    if result["errors"]:
        return json.dumps(result)

    with open("storage.txt", "w") as f:
        json.dump(data, f)

    result["result"] = "success"
    return json.dumps(result)
